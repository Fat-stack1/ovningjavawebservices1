package com.example.ovningspringbootjws.exceptions;

public class AnimalClassificationNotFoundException extends Exception{

    public AnimalClassificationNotFoundException(String id){
        super(id);
    }
}
