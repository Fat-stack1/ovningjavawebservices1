package com.example.ovningspringbootjws.exceptions;

public class AnimalNotFoundException extends Exception{

    public AnimalNotFoundException(String id){
        super(id);
    }

}
