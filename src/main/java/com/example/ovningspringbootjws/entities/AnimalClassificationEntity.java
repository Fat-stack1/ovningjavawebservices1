package com.example.ovningspringbootjws.entities;

import lombok.AllArgsConstructor;
import lombok.Data;



@Data
@AllArgsConstructor
public class AnimalClassificationEntity {
    String classificationName;


}
