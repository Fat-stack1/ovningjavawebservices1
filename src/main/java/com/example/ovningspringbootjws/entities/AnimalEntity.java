package com.example.ovningspringbootjws.entities;

import lombok.*;

import javax.persistence.*;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "animalentity")
public class AnimalEntity {


    @Id
    @Column(name = "id", columnDefinition = "varchar(36)")
    private String id;

    private String name;
    private String binominalName;
    private String description;
    private String conservationStatus;

    public AnimalEntity(String name, String binominalName, String description, String conservationStatus) {
        this.name = name;
        this.binominalName = binominalName;
        this.description = description;
        this.conservationStatus = conservationStatus;
    }
}
