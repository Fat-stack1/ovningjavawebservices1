package com.example.ovningspringbootjws;

import com.example.ovningspringbootjws.repositories.AnimalClassificationRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OvningSpringBootJwsApplication {

    public static void main(String[] args) {
        SpringApplication.run(OvningSpringBootJwsApplication.class, args);



    }

}
