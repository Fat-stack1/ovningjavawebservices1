package com.example.ovningspringbootjws.restcontrollers;

import com.example.ovningspringbootjws.entities.AnimalEntity;
import com.example.ovningspringbootjws.exceptions.AnimalClassificationNotFoundException;
import com.example.ovningspringbootjws.exceptions.AnimalNotFoundException;
import com.example.ovningspringbootjws.models.Animal;
import com.example.ovningspringbootjws.payload.CreateAnimal;
import com.example.ovningspringbootjws.payload.UpdateAnimal;
import com.example.ovningspringbootjws.services.AnimalService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping("/api/animals")
public class AnimalController {

    AnimalService animalService;

    //Lista istället för stream, jobba mot att få det till stream igen.
    @GetMapping
    public List<Animal> all(){

       animalService.all().forEach(AnimalController::toDTO);



    }


    @PostMapping
    public Animal create(@RequestBody CreateAnimal createAnimal){
        return toDTO(animalService.createAnimal(
                createAnimal.getName(),
                createAnimal.getBinominalName())
        );

    }


    @GetMapping("/{id}")
    public ResponseEntity <Animal> get(@PathVariable("id") String id){
        try {
            return ResponseEntity.ok( toDTO(animalService.get(id)) );
        }catch (AnimalNotFoundException e){
            return ResponseEntity.notFound().build();
        }

    }

  @PutMapping("/{id}")
    public ResponseEntity <Animal> update(@PathVariable("id") String id,
                         @RequestBody UpdateAnimal updateAnimal){
        try {
            return ResponseEntity.ok( toDTO(animalService.updateAnimal(
                    id,
                    updateAnimal.getName(),
                    updateAnimal.getBinominalName()
            )));
        }catch (AnimalNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }



    @DeleteMapping("/{id}")
    public ResponseEntity <Void> delete(@PathVariable("id") String id){

        try {
            animalService.delete(id);
            return ResponseEntity.ok().build();

        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping("/{id}/link/{remoteId}")
    public ResponseEntity <Animal> get(@PathVariable("id") String id,
                                       @PathVariable("remoteId") String remoteId){
        try {
            return ResponseEntity.ok( toDTO(animalService.link(id, remoteId)));
        }catch (AnimalNotFoundException e){
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping ("/{id}/classification/{classId}")
    public ResponseEntity<Animal> getWithClass(@PathVariable("id") String id,
                                      @PathVariable("classId") String classId){

        try {
            return ResponseEntity.ok( toDTO(animalService.getWithClass(id, classId)));
        }catch (AnimalNotFoundException | AnimalClassificationNotFoundException e){
            return ResponseEntity.notFound().build();
        }
        
    }




    private static Animal toDTO(AnimalEntity animalEntity) {
        return new Animal(
                animalEntity.getId().toString(),
                animalEntity.getName(),
                animalEntity.getBinominalName(),
                animalEntity.getDescription(),
                animalEntity.getConservationStatus()
        );

    }

}
