package com.example.ovningspringbootjws.payload;

import lombok.Value;

@Value
public class CreateAnimal {
    String name;
    String binominalName;

}
