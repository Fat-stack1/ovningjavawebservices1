package com.example.ovningspringbootjws.payload;

import lombok.Value;

@Value
public class UpdateAnimal {
    String name;
    String binominalName;
}
