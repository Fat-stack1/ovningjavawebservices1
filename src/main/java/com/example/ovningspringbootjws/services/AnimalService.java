package com.example.ovningspringbootjws.services;

import com.example.ovningspringbootjws.JsonPlaceholderRemote;
import com.example.ovningspringbootjws.entities.AnimalClassificationEntity;
import com.example.ovningspringbootjws.entities.AnimalEntity;
import com.example.ovningspringbootjws.exceptions.AnimalClassificationNotFoundException;
import com.example.ovningspringbootjws.exceptions.AnimalNotFoundException;
import com.example.ovningspringbootjws.repositories.AnimalClassificationRepository;
import com.example.ovningspringbootjws.repositories.AnimalRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@AllArgsConstructor
@Service
public class AnimalService {

    AnimalRepository animalRepository;
    JsonPlaceholderRemote jsonPlaceholderRemote;
    AnimalClassificationRepository animalClassificationRepository;


    public List<AnimalEntity> all(){
    return  animalRepository.findAnimalEntityByNameTrue();
    }


    public AnimalEntity createAnimal(String name, String binominalName) {

        AnimalEntity animalEntity = new  AnimalEntity(
                                                    UUID.randomUUID().toString(),
                                                    name,
                                                    binominalName,
                                                    "",
                                                    ""
                                            );
        return animalRepository.save(animalEntity);


    }

    public AnimalEntity get(String id) throws AnimalNotFoundException {
        return animalRepository.findAnimalEntityById(id)
                .orElseThrow(
                        () -> new AnimalNotFoundException(id)
                );


    }

    public AnimalEntity updateAnimal(String id, String name, String binominalName) throws AnimalNotFoundException {
       AnimalEntity animalEntity = animalRepository.findAnimalEntityById(id)
                                   .orElseThrow(() -> new AnimalNotFoundException(id));
       animalEntity.setName(name);
       animalEntity.setBinominalName(binominalName);

       return animalRepository.save(animalEntity);

    }


    public void delete(String id) throws AnimalNotFoundException {
        AnimalEntity animalEntity = animalRepository.findAnimalEntityById(id)
                .orElseThrow(  () -> new AnimalNotFoundException(id));

        animalRepository.delete(animalEntity);
    }


    public AnimalEntity link(String id, String remoteId) throws AnimalNotFoundException{
        AnimalEntity animalEntity = animalRepository.findAnimalEntityById(id)
                .orElseThrow(()-> new AnimalNotFoundException(id));
        JsonPlaceholderRemote.JsonPlaceholder json = jsonPlaceholderRemote.get(remoteId);
        animalEntity.setDescription(json.getBody());
        return animalRepository.save(animalEntity);
    }

    public AnimalEntity getWithClass(String id, String classId) throws AnimalNotFoundException, AnimalClassificationNotFoundException {

        animalClassificationRepository.initClassList();

        AnimalEntity animalEntity = animalRepository.findAnimalEntityById(id)
                .orElseThrow(()-> new AnimalNotFoundException(id));

        AnimalClassificationEntity animalclass = animalClassificationRepository.get(classId)
                        .orElseThrow(()-> new AnimalClassificationNotFoundException(classId));

        System.out.println(animalclass.toString());
        animalEntity.setBinominalName(animalclass.getClassificationName());
        return animalEntity;

    }
}
