package com.example.ovningspringbootjws.repositories;

import com.example.ovningspringbootjws.entities.AnimalEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface AnimalRepository extends JpaRepository<AnimalEntity, String> {



    //Allt i db bör ha ett name=true och därför bör alla returneras.
    List<AnimalEntity> findAnimalEntityByNameTrue();

    Optional<AnimalEntity> findAnimalEntityById(String Id);

 /*   public Optional<AnimalEntity> get(String id) {
        return Optional.ofNullable(animals.get(id));

    }*/




    //public void delete(AnimalEntity animalEntity);
    //{
    //    animals.remove(animalEntity.getId());
    //}
}
