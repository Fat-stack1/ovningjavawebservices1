package com.example.ovningspringbootjws.repositories;

import com.example.ovningspringbootjws.entities.AnimalClassificationEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class AnimalClassificationRepository {
    Map<String, AnimalClassificationEntity> animalClassifications = new HashMap<>();


    public Optional<AnimalClassificationEntity> get(String id) {
        return Optional.ofNullable(animalClassifications.get(id));

    }

    // Metod för att fylla listan, används inte som
    public void initClassList(){
        animalClassifications.put("1", new AnimalClassificationEntity("Däggdjur"));
        animalClassifications.put("2", new AnimalClassificationEntity("Reptildjur"));
        animalClassifications.put("3", new AnimalClassificationEntity("Fågeldjur"));

    }
}
