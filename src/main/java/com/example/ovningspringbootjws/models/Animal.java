package com.example.ovningspringbootjws.models;

import lombok.Value;

@Value
public class Animal {
    String id;
    String name;
    String binominalName;
    String description;
    String conservationStatus;
}
